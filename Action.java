import java.util.Scanner;

public class Action {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Student student1 = new Student("Winston", 2, "Computer Science");

        student1.setAmountLearnt(10);

        Student student2 = new Student("Stuart", 12, "3D Animation & CGI");

        student2.setAmountLearnt(15);

        Student[] section3 = new Student[3];
		section3[0] = student1;
		section3[1] = student2;
		
		//System.out.println(section3[0].name);
		
		section3[2] = new Student("Cadin", 18, "Liberal Arts");
		

        System.out.println("Enter amount studied for " + section3[2].getName() + ": ");
        int amountStudied = scanner.nextInt();
        section3[2].study(amountStudied);

        System.out.println("Name: " + student1.getName());
        System.out.println("Age: " + student1.getAge());
        System.out.println("Program: " + student1.getProgram());
        System.out.println("Amount Learnt: " + student1.getAmountLearnt());

        System.out.println("Name: " + student2.getName());
        System.out.println("Age: " + student2.getAge());
        System.out.println("Program: " + student2.getProgram());
        System.out.println("Amount Learnt: " + student2.getAmountLearnt());

        System.out.println("Name: " + section3[2].getName());
        System.out.println("Age: " + section3[2].getAge());
        System.out.println("Program: " + section3[2].getProgram());
        System.out.println("Amount Learnt: " + section3[2].getAmountLearnt());

        scanner.close();
		
		Student student4 = new Student("Poopy", 19, "Commerce");
		System.out.println("Name: " + student4.getName());
        System.out.println("Age: " + student4.getAge());
        System.out.println("Program: " + student4.getProgram());
        System.out.println("Amount Learnt: " + student4.getAmountLearnt());
    }
}
