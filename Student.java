public class Student {
    private String name;
    private int age;
    private String program;
    private int amountLearnt;

    public Student(String name, int age, String program) {
        this.name = name;
        this.age = age;
        this.program = program;
        this.amountLearnt = 0;
    }
	
    public void sayHi() {
        System.out.println("Hi my program is " + program);
    }

    public void study(int amountStudied) {
        amountLearnt += amountStudied;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public void setAge(int newAge) {
        this.age = newAge;
    }

    public void setProgram(String newProgram) {
        this.program = newProgram;
    }

    public void setAmountLearnt(int newAmountLearnt) {
        this.amountLearnt = newAmountLearnt;
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    public String getProgram() {
        return this.program;
    }

    public int getAmountLearnt() {
        return this.amountLearnt;
    }

    public void sleep() {
        System.out.println(name + " is sleeping.");
    }
}
